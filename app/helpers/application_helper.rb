module ApplicationHelper
  
  # Creates titles for static pages
  def page_title(title = '')
    base_title = "LRSC Football"
    if title.empty?
      base_title
    else
      "#{title} | #{base_title}"
    end
  end
  
end
